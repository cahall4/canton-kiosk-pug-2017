jQuery(document).ready(function ($) {

  var thisSection;
  var thisId;
  var thisInterviews;

  if ($('.btn-pic-tile-video').length) {
    $('.btn-pic-tile-video').bind('click', function(event) {
      thisId = $(this).attr('id');
      thisSection = $(this).attr('name');
      thisInterviews = $(this).attr('title');
      $('body').addClass('show-video');
      $('#interviewTitle').append('<img src="../assets/img/' + thisSection + '/names/' + thisId + '.jpg">');
      $('#interviewVideo').append('<video autoplay src="../assets/vid/' + thisSection + '/' + thisInterviews + '/' + thisId + '.mp4" type="video/mp4" width="320" height="240">');
      return false;
    })
  }

  if ($('#selectAnother').length) {
    $('#selectAnother').bind('click', function(event) {
      $('body').removeClass('show-video');
      $('#interviewTitle img, #interviewVideo embed, #interviewVideo video').remove();
      return false;
    })
  }

});
