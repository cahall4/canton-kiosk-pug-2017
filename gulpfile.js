/*global require*/

// Customize as needed...

// include gulp and required node modules
var gulp = require('gulp'),
	gutil = require('gulp-util'),
	pug = require('gulp-pug'),
	sass = require('gulp-sass'),
  concat = require('gulp-concat'),
	sourcemaps = require('gulp-sourcemaps'),
	connect = require('gulp-connect');

// compile pug
gulp.task('pug', function () {
	gulp.src('./src/pug/templates/**/*.pug')
		.pipe(pug({
			pretty: true
		}))
		.on('error', gutil.log)
		.pipe(gulp.dest('./public/'))
		.pipe(connect.reload());
});

// compile sass
gulp.task('sass', function () {
	gulp.src('./src/sass/styles.scss')
		.pipe(sourcemaps.init())
		.pipe(sass())
		.on('error', gutil.log)
		.pipe(sourcemaps.write('./maps'))
		.pipe(gulp.dest('./public/assets/css/'))
		.pipe(connect.reload());
});

// compile js
gulp.task('js', function () {
	gulp.src(['./src/js/jquery.js', './src/js/plugins.js', './src/js/main.js'])
		.pipe(concat('scripts.js'))
		.on('error', gutil.log)
		.pipe(gulp.dest('./public/assets/js/'))
		.pipe(connect.reload());
});

// live reload
gulp.task('connect', function() {
	connect.server({
		root: 'public',
		livereload: true
	});
});

// watch these files (removed './' because of gaze bug?))
gulp.task('watch', function () {
	gulp.watch(['src/pug/templates/**/*.pug', 'src/pug/layouts/*.pug', 'src/pug/includes/*.pug'], ['pug']);
	gulp.watch(['src/sass/**/*.scss'], ['sass']);
  gulp.watch(['src/js/*.js'], ['js']);
});

// run default task
gulp.task('default', ['pug', 'sass', 'js', 'connect', 'watch']).on('error', gutil.log);
