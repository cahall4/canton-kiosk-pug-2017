CANTON KIOSK REBUILD
====================

The original kiosk was produced in 2000 using Macromedia Director! This version attempts to recreate it using straight up HTML and CSS (no JavaScript) so that it's more portable and future-proof. All of the original content and artwork has been reused, so it looks exactly like the original.

Even though it's plain ol' HTML, we're using some modern web technologies to code and compile everything:

- Pug JS for HTML (https://pugjs.org/)
- SASS for CSS (http://sass-lang.com/)
- Gulp and Node to compile everything (http://gulpjs.com/ and https://nodejs.org/)
- iCab browser for Mac (http://www.icab.de/)

DO NOT EDIT THE HTML AND CSS FILES. Always make edits to the Pug and SASS files in the `src` folder and use Gulp and Node to recompile.

SOME CONSIDERATIONS
-------------------

- In Mac OS 9 file names have a maximum length of 31 characters.
- We're using the old iCab browser because it has a great kiosk mode. It's shareware, so we'll need to purchase a license to make the warning go away. (DONE)
- It looks best at a screen resolution of 640 by 480.
- We tried using JavaScript to enhance the experience, but it kept crashing the browser. In the end we ditched JavaScript in favor of plain ol' HTML. Now it should run on just about anything.
- The only part that does NOT work properly as of now is the credits page. It should scroll with JavaScript, but it does not.

VIDEOS
------

The old videos appear to be using Cinepak compression. In order to have the videos work on the old machine (G3 Tower with Mac OS 9) the Cinepak videos require the .mov extension. The play using the QuickTime plugin. I was able to pull them all from the backup CDs and simply add back the .mov extension -- they all worked fine once I found the correct video.

ARTWORK
-------

All of the artwork was found and reused. We even have the original Photoshop files.